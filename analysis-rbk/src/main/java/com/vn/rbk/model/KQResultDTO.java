package com.vn.rbk.model;

import lombok.Data;

@Data
public class KQResultDTO {
    private String number;
    private String rlst;
}
