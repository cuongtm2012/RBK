/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vn.rbk.controller;

import com.vn.rbk.AppConfig;
import com.vn.rbk.services.base.GoalServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
public class CronGoalController {
    @Autowired
    private GoalServices goalServices;

    @Autowired
    private AppConfig myConfig;

    @Scheduled(cron = "0 0 7,12 * * *")
    public void goalsnow() {
        String URL = "https://www.goalsnow.com/over-under-predictions/";
        goalServices.alsGoalNow(URL);
    }
}
