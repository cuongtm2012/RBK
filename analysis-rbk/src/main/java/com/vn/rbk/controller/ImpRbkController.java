/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.vn.rbk.controller;

import com.sun.syndication.feed.synd.SyndEntryImpl;
import com.sun.syndication.feed.synd.SyndFeed;
import com.sun.syndication.io.SyndFeedInput;
import com.sun.syndication.io.XmlReader;
import com.vn.rbk.AppConfig;
import com.vn.rbk.domain.caudep;
import com.vn.rbk.domain.ketquamnSub;
import com.vn.rbk.domain.ketquamtSub;
import com.vn.rbk.services.base.BatchServices;
import com.vn.rbk.services.base.RbkServices;
import com.vn.rbk.util.DateUtil;
import com.vn.rbk.util.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.net.URL;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Slf4j
@Controller
@RequestMapping("/rbk")
public class ImpRbkController {
    @Autowired
    private RbkServices rbkServices;

    @Autowired
    private BatchServices batchServices;

    @Autowired
    private AppConfig myConfig;

    // Get KQXS from Date to Date DDMMYYYY
    @CrossOrigin
    @GetMapping(value = "/kqxs")
    public ResponseEntity<?> getKQXS(@RequestParam(value = "fromDate", defaultValue = "") String fromDate,
                                     @RequestParam(value = "toDate", defaultValue = "") String toDate) {
        LocalDate fDate = DateUtil.strToDDMMYYYY(fromDate);
        LocalDate tDate = DateUtil.strToDDMMYYYY(toDate);
        while (fDate.compareTo(tDate) < 0) {
            String inputDate = DateUtil.newDateYYYYMMDD(fDate);
            String URL = myConfig.getUrl();
            // return ket qua object
            URL = String.format(URL, inputDate);
            rbkServices.alsKetquasx(URL, inputDate);
            log.info("Import KQXS : " + fDate);
            fDate = DateUtil.datePlusone(fDate);
        }
        return new ResponseEntity<>("Import Success", HttpStatus.OK);
    }

    // Update
    @CrossOrigin
    @PutMapping(value = "/updateKQXS")
    public ResponseEntity<?> updateKQXS(@RequestParam(value = "fromDate", defaultValue = "") String fromDate,
                                        @RequestParam(value = "toDate", defaultValue = "") String toDate) {
        LocalDate fDate = DateUtil.strToDDMMYYYY(fromDate);
        LocalDate tDate = DateUtil.strToDDMMYYYY(toDate);
        while (fDate.compareTo(tDate) < 0) {
            String inputDate = DateUtil.newDateYYYYMMDD(fDate);
            rbkServices.updateKQXS(inputDate);
            fDate = DateUtil.datePlusone(fDate);
        }
        return new ResponseEntity<>("Update Success", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/chotkq")
    public ResponseEntity<?> chotkq(@RequestParam(value = "fromDate", defaultValue = "") String fromDate,
                                    @RequestParam(value = "toDate", defaultValue = "") String toDate) {
        LocalDate fDate = DateUtil.strToDDMMYYYY(fromDate);
        LocalDate tDate = DateUtil.strToDDMMYYYY(toDate);
        while (fDate.compareTo(tDate) < 0) {
            String chotkq = myConfig.getChotkq();
            String todayDateStr = DateUtil.newDateYYYYMMDD(fDate);
            // get data from rongbachkim
            // get list chot ket qua
            chotkq = String.format(chotkq, todayDateStr);
            rbkServices.alsChotKQ(chotkq, todayDateStr);
            fDate = DateUtil.datePlusone(fDate);
        }
        return new ResponseEntity<>("Import Chot Ket Qua Success", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/trending")
    public ResponseEntity<?> trending(@RequestParam(value = "fromDate", defaultValue = "") String fromDate,
                                      @RequestParam(value = "toDate", defaultValue = "") String toDate) {
        LocalDate fDate = DateUtil.strToDDMMYYYY(fromDate);
        LocalDate tDate = DateUtil.strToDDMMYYYY(toDate);
        while (fDate.compareTo(tDate) < 0) {
            String trendURL = myConfig.getTrendURL();
            String todayDateStr = DateUtil.newDateYYYYMMDD(fDate);
            // return trend array
            trendURL = String.format(trendURL, todayDateStr);
            rbkServices.alsTrend(trendURL, todayDateStr);
            fDate = DateUtil.datePlusone(fDate);
        }
        return new ResponseEntity<>("Import Trending Sucess", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/caudep")
    public ResponseEntity<?> caudep() {
        // get limit day
        int limitday = rbkServices.limitCaudep("https://rongbachkim.com/soicau.html");
        String url = myConfig.getCaudepURL();
        Date date = new Date();
        String todayDateStr = DateUtil.dateFormatYYYYMMDD(date);
        // return cau dep array
        for (int limit = 1; limit < limitday; limit++) {
            for (int nhay = 1; nhay < 4; nhay++) {
                for (int lon = 0; lon < 2; lon++) {
                    caudep cd = new caudep();
                    url = String.format(url, todayDateStr, limit, lon, nhay);
                    rbkServices.alsCaudep(cd, url, limit, todayDateStr, nhay, lon);
                    rbkServices.impCaudep(cd);
                }
            }
        }
        return new ResponseEntity<>("Done", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/rssmn")
    public ResponseEntity<?> rssmn() {
        try {
            URL feedSource = new URL("https://xskt.com.vn/rss-feed/mien-nam-xsmn.rss");
            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(new XmlReader(feedSource));
            List object = feed.getEntries();
            for (Object o : object) {
                String ngaychot = rbkServices.parseDate(((SyndEntryImpl) o).getTitle());
                if (Validator.validateString(ngaychot)) {
                    List<ketquamnSub> ketquamnList = rbkServices.parseNumber(((SyndEntryImpl) o).getDescription().getValue(), ngaychot);
                    rbkServices.impkqmn(ketquamnList, ngaychot);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Done", HttpStatus.OK);
    }

    @CrossOrigin
    @GetMapping(value = "/rssmt")
    public ResponseEntity<?> rssmt() {
        try {
            URL feedSource = new URL("https://xskt.com.vn/rss-feed/mien-trung-xsmt.rss");
            SyndFeedInput input = new SyndFeedInput();
            SyndFeed feed = input.build(new XmlReader(feedSource));
            List object = feed.getEntries();
            for (Object o : object) {
                String ngaychot = rbkServices.parseDateMT(((SyndEntryImpl) o).getTitle());
                if (Validator.validateString(ngaychot)) {
                    List<ketquamtSub> ketquamtSubList = rbkServices.parseNumberMT(((SyndEntryImpl) o).getDescription().getValue(), ngaychot);
                    rbkServices.impkqmt(ketquamtSubList, ngaychot);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return new ResponseEntity<>("Done", HttpStatus.OK);
    }
}
