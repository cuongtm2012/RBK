package com.vn.rbk.domain;

import lombok.Data;

@Data
public class goalnow {
    String time;
    String home;
    String away;
    String tip;
    String result;
    String tournament;
    String inpDate;
    long inpTime;
}
