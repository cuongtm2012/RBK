package com.vn.rbk.domain;

import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class caudep {
    String ngaychot;
    String limitnhaylon;
    String thongke;
    List<Map<String, String>> caudeplapList;
}
