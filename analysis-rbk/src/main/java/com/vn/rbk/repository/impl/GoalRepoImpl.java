package com.vn.rbk.repository.impl;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.vn.rbk.MongoConfig;
import com.vn.rbk.domain.goalnow;
import com.vn.rbk.repository.MongoManager;
import com.vn.rbk.repository.base.GoalRepo;
import com.vn.rbk.util.DateUtil;
import com.vn.rbk.util.Validator;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Repository
@Slf4j
public class GoalRepoImpl implements GoalRepo {
    public static final MongoManager mongo = new MongoManager();
    @Autowired
    MongoConfig mongoConfig;

    @Override
    public void insertGoalNow(List<goalnow> goalnowList) {
        try {
            DBCollection gnow = mongo.goalnow(mongoConfig);
            for (goalnow goalnow : goalnowList) {
                BasicDBObject document = new BasicDBObject();
                document.put("time", goalnow.getTime());
                document.put("home", goalnow.getHome());
                document.put("away", goalnow.getAway());
                document.put("tip", goalnow.getTip());
                document.put("result", goalnow.getResult());
                document.put("tournament", goalnow.getTournament());
                document.put("inpdate", goalnow.getInpDate());
                document.put("inptime", goalnow.getInpTime());

                BasicDBObject find = new BasicDBObject();
                find.put("inpdate", goalnow.getInpDate());
                find.put("home", goalnow.getHome());

                int count = mongo.goalnow(mongoConfig).find(find).count();
                if (count < 1) {
                    gnow.insert(document);
                } else {
                    if (Validator.validate(goalnow.getResult())) {
                        BasicDBObject newDocument = new BasicDBObject();
                        newDocument.put("result", goalnow.getResult());
                        BasicDBObject searchQuery = new BasicDBObject().append("home", goalnow.getHome());
                        searchQuery.append("inpdate", goalnow.getInpDate());
                        gnow.update(searchQuery, newDocument);
                    }
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage());
        }
    }

    @Override
    public List<goalnow> listGoalNow(String inpdate, String name) {
        List<goalnow> goalnowList = new ArrayList<>();
        try {
            BasicDBObject document = new BasicDBObject();
            if (Validator.validateString(inpdate)) {
                document.put("inpdate", inpdate);
            } else {
                document.put("inpdate", DateUtil.newDateYYYYMMDD());
            }
            if (Validator.validateString(name)) {
                document.put("name", Pattern.compile(name, Pattern.CASE_INSENSITIVE));
            }

            BasicDBObject sortDocument = new BasicDBObject();
            sortDocument.put("time", -1);

            DBCursor cursor = mongo.goalnow(mongoConfig).find(document).sort(sortDocument);
            while (cursor.hasNext()) {
                goalnow gn = new goalnow();
                DBObject dbobject = cursor.next();
                gn.setTournament((String) dbobject.get("tournament"));
                gn.setTime((String) dbobject.get("time"));
                gn.setHome((String) dbobject.get("home"));
                gn.setAway((String) dbobject.get("away"));
                gn.setTip((String) dbobject.get("tip"));
                gn.setResult((String) dbobject.get("result"));
                gn.setInpDate((String) dbobject.get("inpdate"));
                gn.setInpTime((Long) dbobject.get("inptime"));
                goalnowList.add(gn);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return goalnowList;
    }
}
