package com.vn.rbk.repository.base;

import com.vn.rbk.domain.goalnow;

import java.util.List;

public interface GoalRepo {

    void insertGoalNow(List<goalnow> goalnowList);

    List<goalnow> listGoalNow(String inpdate, String name);
}
